<?php
/**
 * Template Name: Memberships template
 */

get_header();
include('template-parts/page-title-partial.php');
wp_reset_postdata(); 
?>

<div class="section page classes">
    <div class="container">

        <div class="row justify-content-center">
                <?php 
        $loop = new WP_Query( array( 'post_type' => 'vfm_memberships', 'posts_per_page' => 10 ) ); 
        if ( $loop ->have_posts() ) :
        while ( $loop->have_posts() ) : $loop->the_post();
        ?>
            <div class="col-12 col-md-6 col-xl-4">
                <div class="card mb-3 px-0">
                    <div class="p-3 bg-light rectangle-box" style="background-image: url('<?php the_field('image')?>');"></div>
                    <!-- <img src="<?php the_field('image')?>" alt="<?php the_field('title')?>" class="img-fluid card-img-top"> -->
                <div class="card-body">
                    <?php the_title( '<h5 class="card-title text-dark">', '</h5>' ); ?>
                    <p class="big"><?php the_field('subtitle')?></p>
                    <p class="card-text text-dark"><?php the_field('about_class')?></p>
                    <a href="<?php the_field('link')?>" class="btn btn-primary"><?php the_field('button_title')?> </a>
                </div>
                </div>
            </div>
        <?php endwhile; wp_reset_postdata(); 
        else:?>
<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
<?php endif; ?>
        </div>




    </div>
</div>



<?php include('template-parts/cta-partial.php')?>
<?php include('footer.php')?>