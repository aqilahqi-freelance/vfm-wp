<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package vitality-fitness-mma
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
	<meta name="color" content="#ea0000">
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<nav class="navbar navbar-expand-xl navbar-dark bg-dark sticky-top flex-xl-nowrap">
      <div class="cta-navbar d-flex w-100 justify-content-between">
        <!-- <div class="icon-link-group">
          <a href="#"><i class="bi bi-facebook"></i></a>
          <a href="#"><i class="bi bi-instagram"></i></a>
          <a href="#"><i class="bi bi-whatsapp"></i></a>
        </div> -->
		<?php
			wp_nav_menu( array( 
				'theme_location' => 'social',
				'menu_id' => 'social',
				'container' => 'div',
					'menu_class' => 'icon-link-group',
				) ); 
			?>
		<?php
			wp_nav_menu( array( 
				'theme_location' => 'free-trial',
				'menu_id' => 'free-trial',
				) ); 
			?>
        <!-- <a href="#" class="btn cta-navbar-link btn-lg ms-auto" tabindex="-1" role="button"><span>
		
		</span></a> -->
      </div>
        <div class="container">
          <a class="navbar-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/Logo-Full_500px.png" class="img-fluid" alt="logo"></a>

            <button class="navbar-toggler hamburger hamburger--spin" type="button"
        aria-label="Toggle navigation" aria-controls="navigation" data-bs-toggle="collapse" data-bs-target="#mainMenuToggler" aria-controls="mainMenuToggler" aria-expanded="false">
          <span class="hamburger-box">
            <span class="hamburger-inner"></span>
          </span>
        </button>
            <div class="collapse navbar-collapse" id="mainMenuToggler">
			<?php
				wp_nav_menu( array( 
					'theme_location' => 'menu-1',
					'menu_id' => 'primary-menu',
					'container' => 'ul',
					'menu_class' => 'navbar-nav ms-auto mb-2 mb-lg-0',
					// 'menu_id' => ' ',
					) ); 
				?>
				<!-- <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
					<li class="nav-item d-flex">
					<a class="nav-link ms-auto active" aria-current="page" href=".">Home</a>
					</li>
					<li class="nav-item d-flex">
					<a class="nav-link ms-auto" href="about.php">About</a>
					</li>
					<li class="nav-item d-flex">
					<a class="nav-link ms-auto" href="classes.php">Classes</a>
					</li>
				</ul> -->
            </div>
        </div>
    </nav>
<div id="page" class="site">
