<?php
/**
 * vitality-fitness-mma functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package vitality-fitness-mma
 */

if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '1.0.0' );
}

if ( ! function_exists( 'vfm_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function vfm_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on vitality-fitness-mma, use a find and replace
		 * to change 'vfm' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'vfm', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus(
			array(
				'menu-1' => esc_html__( 'Primary', 'vfm' ),
				'free-trial' => esc_html__( 'Free Trial', 'vfm' ),
				'social' => esc_html__( 'Social', 'vfm' ),
				'footer' => esc_html__( 'Footer', 'vfm' ),
			)
		);

		// Custom post
		add_action( 'init', 'create_post_type' );
		function create_post_type() {
			register_post_type( 'vfm_classes',
				array(
					'labels' => array(
						'name' => __( 'Classes' ),
						'singular_name' => __( 'Class' ),
						'all_items' => __( 'All Classess'),
						'view_item' => __( 'View Class'),
						'add_new_item' => __( 'Add New Class'),
						'edit_item' => __( 'Edit Class'),
						'update_item'  => __( 'Update Class'),
						'search_items'        => __( 'Search Class'),
					),
					'menu_icon' => 'dashicons-megaphone',
					'public' => true,
					'has_archive' => true,
				)
			);
			register_post_type( 'vfm_programs',
				array(
					'labels' => array(
						'name' => __( 'Programs' ),
						'singular_name' => __( 'Program' ),
						'all_items' => __( 'All Programs'),
						'view_item' => __( 'View Program'),
						'add_new_item' => __( 'Add New Program'),
						'edit_item' => __( 'Edit Program'),
						'update_item'  => __( 'Update Program'),
						'search_items'        => __( 'Search Program'),
					),
					'menu_icon' => 'dashicons-book-alt',
				'public' => true,
				'has_archive' => true,
				)
			);
			register_post_type( 'vfm_products',
				array(
					'labels' => array(
						'name' => __( 'Products' ),
						'singular_name' => __( 'Product' ),
						'all_items' => __( 'All Products'),
						'view_item' => __( 'View Product'),
						'add_new_item' => __( 'Add New Product'),
						'edit_item' => __( 'Edit Product'),
						'update_item'  => __( 'Update Product'),
						'search_items'        => __( 'Search Product'),
					),
					'menu_icon' => 'dashicons-cart',
					'public' => true,
					'has_archive' => true,
				)
			);
			register_post_type( 'vfm_memberships',
				array(
					'labels' => array(
						'name' => __( 'Memberships' ),
						'singular_name' => __( 'Membership' ),
						'all_items' => __( 'All Memberships'),
						'view_item' => __( 'View Membership'),
						'add_new_item' => __( 'Add New Membership'),
						'edit_item' => __( 'Edit Membership'),
						'update_item'  => __( 'Update Membership'),
						'search_items'        => __( 'Search Membership'),
					),
					'menu_icon' => 'dashicons-clipboard',
				'public' => true,
				'has_archive' => true,
				)
			);
			register_post_type( 'vfm_testimonials',
			array(
				'labels' => array(
					'name' => __( 'Testimonials' ),
					'singular_name' => __( 'Testimony' ),
					'all_items' => __( 'All Testimonials'),
					'view_item' => __( 'View Testimonial'),
					'add_new_item' => __( 'Add New Testimonial'),
					'edit_item' => __( 'Edit Testimonial'),
					'update_item'  => __( 'Update Testimonial'),
					'search_items'        => __( 'Search Testimonial'),
				),
				'menu_icon' => 'dashicons-format-quote',
			'public' => true,
			'has_archive' => true,
			)
		);
		}

		// Disable page editor in template
		add_action( 'admin_init', 'hide_editor' );
 
		function hide_editor() {
			if ( isset ( $_GET['post'] ) )
				$post_id = $_GET['post'];
			else if ( isset ( $_POST['post_ID'] ) )
				$post_id = $_POST['post_ID'];

			if( !isset ( $post_id ) || empty ( $post_id ) )
			return;
		
			$template_file = get_post_meta($post_id, '_wp_page_template', true);
			
			if($template_file == 'fontpage.php' || $template_file == 'classes.php' || $template_file == 'about.php' || $template_file == 'memberships.php' || $template_file == 'program.php' || $template_file == 'products.php'){ // edit the template name
				remove_post_type_support('page', 'editor');
			}
		}
		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
			)
		);

		// Set up the WordPress core custom background feature.
		add_theme_support(
			'custom-background',
			apply_filters(
				'vfm_custom_background_args',
				array(
					'default-color' => 'ffffff',
					'default-image' => '',
				)
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 250,
				'width'       => 250,
				'flex-width'  => true,
				'flex-height' => true,
			)
		);
	}
endif;
add_action( 'after_setup_theme', 'vfm_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function vfm_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'vfm_content_width', 640 );
}
add_action( 'after_setup_theme', 'vfm_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function vfm_widgets_init() {
	register_sidebar(
		array(
			'name'          => esc_html__( 'Sidebar', 'vfm' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'vfm' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}
add_action( 'widgets_init', 'vfm_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function vfm_scripts() {
	// wp_enqueue_style( 'vfm-style', get_stylesheet_uri(), array(), _S_VERSION );
	// wp_style_add_data( 'vfm-style', 'rtl', 'replace' );

	// wp_enqueue_script( 'vfm-navigation', get_template_directory_uri() . '/js/navigation.js', array(), _S_VERSION, true );
	wp_enqueue_style( 'vfm-custom-style', get_template_directory_uri() . '/assets/css/main.css' );
	wp_enqueue_script( 'vfm-js', get_template_directory_uri()  . '/assets/js/main.js');

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'vfm_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

