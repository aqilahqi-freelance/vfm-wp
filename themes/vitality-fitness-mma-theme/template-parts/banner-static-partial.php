<div class="banner full-height" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/img/gym-space-6.jpg');">
    <div class="container">
        <div class="text-wrapper">
            <h1 class="text-uppercase">Be a part of the gang</h1>
            <p class="big">Enjoy amazing benefits when you workout with us!</p>
        </div>
        <a href="#" class="btn btn-primary">get a membership</a>
    </div>
</div>