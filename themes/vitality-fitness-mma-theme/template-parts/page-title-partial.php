<div class="section title">
    <div class="container">
        <div class="text-wrapper">
            <!-- <h5 class="text-uppercase">About </h5> -->
            <?php 
            if(get_field('sub_title')){
                ?><h5 class="text-uppercase"><?php the_field('sub_title')?></h5><?php }
            the_title( '<h1 class="entry-title text-uppercase">', '</h1>' ); 
            // the_content();
            ?>
        </div>
    </div>
</div>