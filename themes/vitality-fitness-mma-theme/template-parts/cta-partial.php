<?php
/**
 * Template Name: Call-to-Action
 */?>
<div class="section cta bg-center-cover" style="background-image: url('<?php the_field('background_image', 84); ?>');">
    <div class="text-wrapper">
        <h2 class="text-uppercase"><?php the_field('call-to-action_title'); ?></h2>
        <?php
                $cta = get_field('call-to-action_title', 84);
                
                if( $cta ) {
                    ?><h2 class="text-uppercase"><?php the_field('call-to-action_title', 84); ?></h2><?php
                }
            ?>
        <a href="<?php
        if(get_field('go_to', 84)){
            the_field('page_link', 84);
        }else {
            the_field('external_link', 84);
        }
        ?>" class="btn btn-primary"><?php the_field('button_title', 84); ?></a>
    </div>
</div>