<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package vitality-fitness-mma
 */

?>

    <!-- Footer -->
    <footer class="footer mt-auto py-3">
      <div class="container">
        <!-- <div class="d-flex justify-content-center">
          <p class="mb-0">Copyright &copy; 2021 Vitality Fitness MMA</p>
        </div> -->
        <div class="d-lg-flex justify-content-between align-items-start">
          <img src="<?php echo get_template_directory_uri(); ?>/assets/img/Logo-Full_500px.png" class="img-fluid" alt="Logo">
          <div class="row g-2 justify-content-end">
            <!-- <div class="col-12 col-md-4 col-lg-3">
              <div class="p-3">
                <h5>Explore</h5>
                <ul class="list-unstyled">
                  <li><a href="#">Membership</a></li>
                  <li><a href="#">Classes</a></li>
                  <li><a href="#">Personal Training</a></li>
                  <li><a href="#">Products</a></li>
                </ul>
              </div>
            </div> -->
            <div class="col-12 col-md-4 col-lg-3">
              <div class="p-3">
                <h5>Explore</h5>
                <?php
                  wp_nav_menu( array( 
                    'theme_location' => 'footer',
                    'menu_id' => 'footer',
                    'container' => 'ul',
                      'menu_class' => 'list-unstyled',
                    ) ); 
                  ?>
              </div>
            </div>
            <div class="col-12 col-md-4">
              <div class="p-3">
                <h5>Follow Us</h5>
                <p>No 1, Jalan Boling Padang H 13/H, Seksyen 13, 40100 Shah Alam, Selangor</p>
                <p>Operation hours : 8:00am -10:00pm</p>
                <?php
			wp_nav_menu( array( 
				'theme_location' => 'social',
				'menu_id' => 'social',
				'container' => 'div',
					'menu_class' => 'icon-link-group',
				) ); 
			?>

              </div>
            </div>
          </div>
        </div>
      </div>
    </footer>

<?php wp_footer(); ?>

</body>
</html>
