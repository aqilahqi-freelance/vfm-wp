<?php
/**
 * Template Name: Homepage
 */

get_header();
include('template-parts/banner-static-partial.php');
?>

<!-- START: body -->


<div class="section intro float-up">
    <div class="float-box">
        <div class="text-wrapper">
            <h5 class="text-uppercase">Welcome to</h5>
            <h1 class="text-capitalize">Vitality Fitness Mixed Martial Arts</h1>
            <p>We are modern & performance oriented fitness centre that focus on result & outstanding performance. We provide variety of sports program such as loose weight & gain weight programme, athletic strength and conditioning programme, muaythai class & ladies Sculpting & Brazilian butt program.</p>
        </div>
    </div>
</div>

<div class="section features full-height bg-center-cover" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/img/gym-space-1.jpg');">
    <div class="container">
        <div class="row justify-content-end">
            <div class="text-wrapper">
                <h1 class="text-uppercase">We have <br>what you need</h1>
                <h5 class="text-uppercase">don't miss out</h5>
            </div>
        </div>
        <div class="row">
            <div class="feature-wrapper">
                <div class="feature-box">
                    <a href="#" class="feature-link">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/icon-muscle.png" class="img-fluid icon" alt="icon">
                        <p class="heading fw-bold">Membership</p>
                    </a>
                </div>
                <div class="feature-box">
                    <a href="#" class="feature-link">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/icon-calendar.png" class="img-fluid icon" alt="icon">
                        <p class="heading fw-bold">Classes</p>
                    </a>
                </div>
                <div class="feature-box">
                    <a href="#" class="feature-link">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/icon-fitness.png" class="img-fluid icon" alt="icon">
                        <p class="heading fw-bold">Programme</p>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="section testimonial">
    <div class="container">
        <div class="row align-items-stretch">
            <div class="col-12 col-lg-6 p-5">
                 <div class="ratio ratio-1x1">
                <iframe src="https://www.youtube.com/embed/zpOULjyy-n8?rel=0" title="YouTube video" allowfullscreen></iframe>
                </div>
            </div>
            <div class="col-12 col-lg-6 d-flex align-items-center justify-content-center">
                <div class="text-wrapper">
                    <div class="title text-capitalize"><h3>Personal Training</h3></div>
                    <div class="small-title text-uppercase"><p>weight loss</p></div>
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/icon-left-quote.png" alt="quote" class="img-fluid quote">

                    <p class="testimony">Saya sudah berhenti mengambil ubat diabetic sejak saya enroll personal training di Vitalit Fitness & MMA</p>

                    <h5 class="customer-name"><span></span>Akak</h5>
                </div>
            </div>
        </div>
        <div class="d-flex justify-content-end">
        <a href="#" class="btn btn-primary ms-auto">more testimonials <i class="bi bi-arrow-right"></i></a>
        </div>
    </div>
</div>

<!-- END: body -->

<?php
get_footer();