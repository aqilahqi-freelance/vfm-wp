<?php
/**
 * Template Name: About template
 */

get_header();
include('template-parts/page-title-partial.php');
?>

<div class="section page about">
    <div class="container overflow-hidden align-items-stretch">
    <div class="row gy-3">
        <div class="col-md-4 d-flex">
        <div class="p-5 p-md-3 p-lg-5 bg-light w-100 d-flex align-items-center">
            <div class="text-wrapper">
            <?php
                $title_top = get_field('title_top');
                
                if( $title_top ) {
                    ?><h3><?php the_field('title_top'); ?></h3><?php
                }


                $content_top = get_field('content_top');
                if( $content_top ) {
                    ?><p><?php the_field('content_top'); ?></p><?php
                }
            ?>
            </div>
        </div>
        </div>
        <div class="col-md-8">
            <div class="row gy-3">
            <div class="col-6">
                <div class="p-3 bg-light square-box" 
                <?php if( get_field('image_2') ): ?>
                    style="background-image: url('<?php the_field('image_2'); ?>');"
                <?php endif; ?>
                ></div>
            </div>
            <div class="col-6">
            <div class="p-3 bg-light square-box" 
            <?php if( get_field('image_3') ): ?>
                    style="background-image: url('<?php the_field('image_3'); ?>');"
                <?php endif; ?>
            ></div>
            </div>
            <div class="col-6">
            <div class="p-3 square-box">
                <h2 class="heading">We are the most active gym in Shah Alam</h2>
            </div>
            </div>
            <div class="col-6">
            <div class="p-3 bg-light square-box" 
                <?php if( get_field('image_4') ): ?>
                    style="background-image: url('<?php the_field('image_4'); ?>');"
                <?php endif; ?>
            ></div>
            </div>
            </div>
        </div>
    </div>
    <div class="row gy-3 mt-1 mt-md-3 mt-xl-1">
        <div class="col-md-4" >
            <div class="py-5 bg-light square-box full" 
            <?php if( get_field('image_1') ): ?>
                    style="background-image: url('<?php the_field('image_1'); ?>');"
                <?php endif; ?>
            ></div>
        </div>
        <div class="col-md-8">
            <div class="p-5 p-md-3 p-lg-5 bg-light rectangle-box">
                <div class="text-wrapper">
                <?php
                    $content_bottom = get_field('content_bottom');
                    
                    if( $content_bottom ) {
                        ?><p><?php the_field('content_bottom'); ?></p><?php
                    }


                    $button_bottom = get_field('button_bottom');
                    if( $button_bottom ) {
                        ?><a href="<?php the_field('button_bottom'); ?>" class="btn btn-primary"><?php the_field('button_bottom_title'); ?></a><?php
                    }
                ?>

                </div>
            </div>
        </div>
    </div>
    </div>
</div>

<?php include('template-parts/cta-partial.php')?>
<?php include('footer.php')?>