const { src, dest, watch, series } = require('gulp');
const sass = require('gulp-sass');
const sassGlob = require('gulp-sass-glob');
const uglify = require('gulp-uglify');
const uglifycss = require('gulp-uglifycss');
const concat = require('gulp-concat');
const autoprefixer = require('gulp-autoprefixer');
const sourcemaps = require('gulp-sourcemaps');
const browserSync = require('browser-sync').create();

/**  Note 
src - Point to files to use
dest - Points to folder to output
watch - Watch files and folders for changes
*/

/** Minify JS */
const minifyJS = () => {
    return src('src/js/*.js')
    .pipe(uglify())
    .pipe(dest('assets/js'));
}

/** Minify CSS */
const minifyCss = () => {
    return src(['assets/css/*.css', 'src/css-vendors/*.css'])
    .pipe(concat('main.css'))
    .pipe(uglifycss({
    "maxLineLen": 80,
    "uglyComments": true
    }))
    .pipe(dest('assets/css'));
}


/** Compile sass */
const compileSass = () => {
    return src('src/sass/main.scss')
    .pipe(sassGlob())
    .pipe(sourcemaps.init())
    .pipe( sass({
            errorLogToConsole: true,
            outputStyle: 'compressed'
    }) )
    .on( 'error', console.error.bind( console ))

    .pipe(autoprefixer({
        cascade: false
    }))
    .pipe(sourcemaps.write('.'))
    .pipe(dest('assets/css'));
}

/* Watch sass Only */
const watchSass = () => {
    watch('src/sass/**/*.scss', compileSass);
};


/** Concat all files */ 
const compileJS = () => {
    return src([
    // 'node_modules/jquery/dist/jquery.js',
    'node_modules/@popperjs/core/dist/umd/popper.js',
    'node_modules/bootstrap/dist/js/bootstrap.js',
    'src/js/*.js'])
    .pipe(concat('main.js'))
    .pipe(uglify())
    .pipe(dest('assets/js'));
};


/** Browser Sync */
const serve = () => {
    browserSync.init({
        /**  change local url accordingly
         * proxy: "https://html-workflow.local/
         */  
        proxy: "http://localhost/vitality-fitness-mma-wp/"  
    });
    watch('src/sass/**/*.scss', compileSass); 
    watch("src/js/main.js", compileJS);
    watch("./**/*.php").on('change', browserSync.reload);
    watch("./template-parts/**/*.php").on('change', browserSync.reload);
    watch("./assets/css/main.css").on('change', browserSync.reload);
    watch("./assets/js/main.js").on('change', browserSync.reload);
};

// /** Default Task: Browser sync */
// gulp.task('default', ['serve']);
// gulp.task('build', ['scripts', 'sass']);

// exports.minifyJS = minifyJS;
exports.minifyCss = minifyCss;
exports.compileSass = compileSass;
exports.compileJS = compileJS;
exports.watchSass = watchSass;

exports.default = serve;
exports.build = series( compileSass, compileJS)