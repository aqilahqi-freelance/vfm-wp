## HTML Boiler Plate for Wordpress Templating ##
* This is a standard workflow for html development
* Version 2.0 :
* fixed depricated @import for sass

## Prerequisite: What You Need ##
* Node
* NPM

## Install Gulp ##
* Open terminal
* Run "npm install --global gulp-cli" to install gulp CLI globally

## Run Gulp ##
* Open gulpfile.js, on line 79, change url
* Open terminal
* "gulp" to run
* "gulp build" to compile sass and js files only

## HTML ##
* change theme color meta in header
* Other pages to remain in main folder
* Partial templates will be in /template-parts folder

## SASS ##
* please refer to main.scss for bootstrap and 'class naming' guidelines
* never use _shame.scss unless a new developer takes over this project and is unsure where to add/change the styling
* use 'gulp sass-pages' to compile sass files for individual pages