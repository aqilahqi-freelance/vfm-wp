<?php
/**
 * Template Name: Classes template
 */

get_header();
include('template-parts/page-title-partial.php');
wp_reset_postdata(); 
?>

<div class="section page classes">
    <div class="container">
    <?php 
        $loop = new WP_Query( array( 'post_type' => 'vfm_classes', 'posts_per_page' => 10 ) ); 
        if ( $loop ->have_posts() ) :
        while ( $loop->have_posts() ) : $loop->the_post();
        ?>
        <div class="row align-items-stretch">
            <div class="col-12 col-md-6 img-wrapper">
                <div class="p-3 bg-light square-box" style="background-image: url('<?php the_field('image')?>');"></div>
            </div>
            <div class="col-12 col-md-6 d-flex align-items-center">
                <div class="text-wrapper p-5 p-md-3 p-lg-5">
                <?php the_title( '<h2 class="text-uppercase">', '</h2>' ); ?>
                    <!-- <h2 class="text-uppercase">Muay Thai</h2> -->
                    <p class="big"><?php the_field('subtitle')?></p>
                    <p><?php the_field('about_class')?></p>
                    <a href="<?php the_field('link')?>" class="btn btn-primary"><?php the_field('button_title')?> <i class="bi bi-whatsapp"></i></a>
                </div>
            </div>
        </div>


        <?php endwhile; wp_reset_postdata(); 
        else:?>
<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
<?php endif; ?>

    </div>
</div>



<?php include('template-parts/cta-partial.php')?>
<?php include('footer.php')?>